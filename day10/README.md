# Day 10

The program reads in all input lines, and from the *value* lines
creates the initial state, a map from bot ids to a vector of one or two
values, and from the *bot* lines creates an action map from bot ids to
the two targets, another bot or an *output*, that will be the
recipients of the two values the bot has.

The main loop starts with the initial state, then finds a bot that has
two values, looks up the action for it, then updates the state by
adding the two values to the respective targets and removing the bot
from the state since it will not have any further actions.

In order to handle both bots and outputs in the same state, and given
that the ids of bots and outputs are both natural numbers, we 
represent the output ids to be a one less than the negative of the id,
ie, output id of 0 maps to -1, 1 to -2 etc.

The above encoding (hack?) could be avoided by creating a data type or
structure to distinguish them.  One approach would be to use two
record types as was done in the initial version.  However this does
introduce overhead when processing the actions, so was discard in
this version in favor of using negative numbers.

## To Run from the Command Line

lein run input

lein test

