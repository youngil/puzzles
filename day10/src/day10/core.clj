(ns day10.core
  (:require [clojure.string :as str]))

(defn bot-or-output
  "Return `id` if `tag` is bot, negative of `id` for output."
  [tag id]
  (let [i (Integer/parseInt id)]
    (case tag
      "bot" i
      "output" (- (inc i)))))

;;; regex to extract the relevant fields from the `bot` or `value`
;;; instructions
(def bot-re #"bot (\d+) .+ (bot|output) (\d+) .+ (bot|output) (\d+)")
(def val-re #"value (\d+) .+ bot (\d+)")

(defn initial-state
  "Parse input `lines` for value actions and return the state that maps a
  bot id to a vector of values that holds it holds of 0, 1 or 2
  values."
  [lines]
  (reduce (fn [new-map line]
            (if-let [[_ val id] (re-find val-re line)]
              (let [v (Integer/parseInt val)
                    i (Integer/parseInt id)]
                (if-let [x (new-map i)]
                  (assoc new-map i (conj x v))
                  (assoc new-map i [v])))
              new-map))
          {}
          lines))


(defn bot-to-action-map
  "Parse input `lines` for bot actions and return a map from bot id to
  action for that bot.  Assume that each bot has only one action, else
  the later action will override."
  [lines]
  (reduce (fn [new-map line]
            (if-let [[_ id l-type l-id h-type h-id] (re-find bot-re line) ]
              (assoc new-map
                     (Integer/parseInt id)
                     [(bot-or-output l-type l-id) (bot-or-output h-type h-id)])
              new-map))
          {}
          lines))

(defn bots-with-two-values
  "Return a sequence of bots (id >=0) and has two values from the `state`."
  [state]
  (filter #(and (> (count (second %)) 1) (>= (first %) 0))
          state))

(defn next-state
  "Compute the next state given the currrent `state`, a bot with `id`
  and two values, `v1` and `v2`, and the recipients for the low and
  high values `l-id`and `h-id`, by removing the bot and adding the
  values to recipients' values."
  [state [id [v1 v2]] [l-id h-id]]
  (let [min-val (min v1 v2)
        max-val (max v1 v2)]
    (when (and (= min-val 17) (= max-val 61))
      (println "Day 4 Part 1: Found: bot" id "compares" min-val "and" max-val))
    (-> state
        (dissoc id)
        (update l-id #(if % (conj % min-val) [min-val]))
        (update h-id #(if % (conj % max-val) [max-val])))))

(defn -main
  "Load instructions from `input-file`, initialize the bots from value
  commands. Next, select a bot that can run and update the state of bots and
  output, and loop until no bot can act."
  [input-file]
  (let [lines (str/split-lines (slurp input-file))
        id-to-action (bot-to-action-map lines)]
    (loop [state (initial-state lines)]
      (if-let [enabled (seq (bots-with-two-values state))]
        (recur (next-state state (first enabled) (id-to-action (ffirst enabled))))
        (println "Day 4 Part 2: The product of the values in outputs 0, 1, and 2 is"
                 (apply * (mapcat state [-1 -2 -3])))))))

