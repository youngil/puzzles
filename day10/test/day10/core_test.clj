(ns day10.core-test
  (:require [clojure.test :refer :all]
            [day10.core :refer :all]
            [clojure.test.check :as tc]
            [clojure.test.check.clojure-test :refer (defspec)]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))

;;;; test.check
(def id gen/nat)
(def values gen/pos-int)
(def target-type (gen/elements ["bot" "output"]))

(def value-template "value %d goes to bot %d")
(def bot-template "bot %d gives low to %s %d and high to %s %d")

(defspec parse-line-test 5
  (prop/for-all [v values
                 bot-id id
                 id1 id
                 id2 id
                 t1 target-type
                 t2 target-type]
                (let [v-line-in (format value-template v bot-id)
                      [v-line-out val-out bot-id-out] (re-find val-re v-line-in)
                      b-line-in (format bot-template bot-id t1 id1 t2 id2)
                      [b-line-out b-id t1-out id1-out t2-out id2-out] (re-find bot-re b-line-in)]
                  (and (= v-line-in v-line-out)
                       (= (str v) val-out)
                       (= (str bot-id) bot-id-out)
                       (= b-line-in b-line-out)
                       (= t1 t1-out)
                       (= t2 t2-out)
                       (= (str id1) id1-out)
                       (= (str id2) id2-out)))))

(defspec initial-state-test 5
  (prop/for-all [v1 values
                 v2 values
                 [bot1 bot2] (gen/vector-distinct id {:num-elements 2})]
                (let [v-line1 (format value-template v1 bot1)
                      v-line2 (format value-template v2 bot2)
                      state (initial-state [v-line1 v-line2])]
                  (and (= (state bot1) [v1])
                       (= (state bot2) [v2])))))

(defspec bot-to-action-map-test 5
  (prop/for-all [v values
                 bot-id id
                 id1 id
                 id2 id
                 t1 target-type
                 t2 target-type]
                (let [b-line-in (format bot-template bot-id t1 id1 t2 id2)
                      action-map (bot-to-action-map [b-line-in])
                      [l-id h-id] (action-map bot-id)]
                  (and (= (bot-or-output t1 (str id1)) l-id)
                       (= (bot-or-output t2 (str id2)) h-id)))))

(defspec bots-with-two-values-test 5
  (prop/for-all [[v1 v2 v3 v4 v5] (gen/vector-distinct values {:num-elements 5})
                 [bot1 bot2 bot3] (gen/vector-distinct id {:num-elements 3})]
                (let [state { bot1 [v1 v2] bot2 [v3] bot3 [v4 v5]}]
                  (= (seq [[bot1 [v1 v2]] [bot3 [v4 v5]]])
                     (bots-with-two-values state)))))

(defspec next-state-test 5
  (prop/for-all [[v1 v2 v3] (gen/vector-distinct values {:num-elements 3})
                 [bot1 bot2 bot3] (gen/vector-distinct id {:num-elements 3})]
                (let [state { bot1 [v1 v2] bot2 [v3] }
                      bot [bot1 [v1 v2]]
                      targets [bot2 bot3]]
                  (= { bot2 [v3 (min v1 v2)] bot3 [(max v1 v2)] }
                     (next-state state bot targets)))))

;;;; clojure.test
(deftest bot-or-output-test
  (is (= 12 (bot-or-output "bot" "12")))
  (is (= -13 (bot-or-output "output" "12"))))

(deftest parse-line-test
  (is (= ["value 5 goes to bot 2" "5" "2"]
         (re-find val-re "value 5 goes to bot 2")))
  (is (= ["bot 2 gives low to bot 1 and high to bot 0" "2" "bot" "1" "bot" "0"]
         (re-find bot-re "bot 2 gives low to bot 1 and high to bot 0")))
  (is (= ["bot 2 gives low to bot 1 and high to output 0" "2" "bot" "1" "output" "0"]
         (re-find bot-re "bot 2 gives low to bot 1 and high to output 0"))))

(deftest initial-values-test
  (is (= { 0 [11 12] 1 [21] }
         (initial-state ["value 11 goes to bot 0"
                         "bot 0 gives low to bot 1 and high to bot 2"
                         "value 21 goes to bot 1"
                         "bot 1 gives low to bot 2 and high to bot 0"
                         "value 12 goes to bot 0"]))))

(deftest bot-to-action-map-test
  (is (= { 0 [1 2] 1 [2 0] }
         (bot-to-action-map ["value 11 goes to bot 0"
                             "bot 0 gives low to bot 1 and high to bot 2"
                             "value 21 goes to bot 1"
                             "bot 1 gives low to bot 2 and high to bot 0"
                             "value 12 goes to bot 0"]))))

(deftest bots-with-two-values-test
  (let [state { 0 [11 12] 1 [21] 2 [31 32]}]
    (is (= (seq [ [0 [11 12]] [2 [31 32]] ])
           (bots-with-two-values state)))))

(deftest next-state-test
  (let [state { 0 [11 12] 1 [21] }
        bot [0 [11 12]]
        targets [1 -1]]
    (is (= { 1 [21 11] -1 [12] }
           (next-state state bot targets)))))

