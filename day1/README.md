# Day 1

The program first creates a sequence of positions including all the
intermediate intersections following the instructions.

The answer to the first part is just the manhattan distance from the
origin to the last position.

To answer the second part we need to check whether any of the
positions have been visited before in order.  To do this we use
recursion to loop over the positions in order with a map of previous
positions that is updated each time we loop.

A *position* is represented as a tuple [h x y] where h is one of :N :E
:S :W and x and y are integer coordinates.

An *instruction* is represented as a tuple ["R12" "R" "12"]. The first
item is not used, but kept since the *re-seq* returns it along with
the other matches.

NB. For the first part, just to compute the final position, we could
have done a simple reduce with a function that only computed the next
position, rather than including all the intermediate points, but since
the sequence of all positions was required for the second part, we
use that.

## To Run from Command Line

lein run input

lein test
