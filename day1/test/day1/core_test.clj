(ns day1.core-test
  (:require [clojure.test :refer :all]
            [day1.core :refer :all]
            [clojure.test.check :as tc]
            [clojure.test.check.clojure-test :refer (defspec)]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))

;;; clojure.test
(deftest move-test
  (testing "move"
    (is (= [[:N 0 0] [:E 1 0] [:E 2 0]]
           (move [[:N 0 0]] ["R2" "R" "2"])))))

(deftest positions-test
  (testing "positions"
    (is (= [[:N 0 0] [:E 1 0] [:E 2 0] [:S 2 -1] [:S 2 -2] [:E 3 -2]]
           (all-positions (re-seq #"(R|L)(\d+)" "R2, R2, L1"))))))

;;; test.check
(def small-number (gen/choose 1 5))
(def turn (gen/elements ["R" "L"]))
(def tuple (gen/tuple turn small-number))
(def action-string (gen/fmap (fn [[t d]] (str t d)) (gen/tuple turn small-number)))
(def vec-actions (gen/not-empty (gen/vector action-string 1 5)))
(def input-string (gen/let [a vec-actions] (clojure.string/join ", " a)))

(defspec all-positions-test 5
  (prop/for-all [in input-string]
                (let [instructions (parse-input in)
                      result (all-positions instructions)
                      [h x y] (last result)]
                  (and (= [:N 0 0] (first result))
                       (> (count result) (count instructions))
                       (h #{:N :E :S :W})
                       (integer? x)
                       (integer? y)))))

(defspec move-test 5
  (prop/for-all [aa action-string]
                (let [result (move [[:N 0 0]] (first (parse-input aa)))
                      [h x y] (last result)]
                  (and (> (count result) 1)
                       (h #{:N :E :S :W})
                       (integer? x)
                       (integer? y)))))



