(ns day1.core)

(defn move
  "Return the vector of `positions` with all intermediate points to the
  new position added according to `instruction`."
  [positions instruction]
  (let [[heading x y] (last positions)
        [_ turn blocks] instruction
        d (Integer/parseInt blocks)]
    (into positions 
          (let [d-range (range 1 (inc d))] ; include the end point d in range
            (case [heading turn]
              ([:N "R"] [:S "L"]) (for [n d-range] [:E (+ x n) y])
              ([:N "L"] [:S "R"]) (for [n d-range] [:W (- x n) y])
              ([:E "R"] [:W "L"]) (for [n d-range] [:S x (- y n)])
              ([:E "L"] [:W "R"]) (for [n d-range] [:N x (+ y n)]))))))
          
(defn all-positions
  "Return vector of all positions visited, starting from (0 0) with intial heading
  of North following the sequence of `instructions`."
  [instructions]
  (reduce move
          [[:N 0 0]]
          instructions))

(defn first-revisited
  "Find the first revisited position in `positions` by maintaining a set
  of visited locations. If no such position found, then print message
  and return the origin."
  [positions]
  (loop [pos positions
         visited #{}]
    (if (seq pos)
      (let [[h x y] (first pos)]
        (if (visited [x y])
          [h x y]
          (recur (rest pos) (conj visited [x y]))))
      (do
        (println "No revisited position found!")
        [:N 0 0]))))

(defn manhattan-distance [[_ x y]] (+ (Math/abs x) (Math/abs y)))

(defn parse-input
  [line]
  (re-seq #"(R|L)(\d+)" line))

(defn -main
  "Read the instructions from `input-file` then print the manhattan
  distance to (a) the final position and (b) to the first revisited
  position following the instructions from the input file provided on
  the command line."
  [input-file]
  (let [positions (->> input-file
                       slurp
                       parse-input
                       all-positions)]
    (println "Day 1 Part 1: Distance to final position:"
             (-> positions
                 last
                 manhattan-distance))
    (println "Day 1 Part 2: Distance to Bunny HQ:"
             (-> positions
                 first-revisited
                 manhattan-distance))))
