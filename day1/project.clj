(defproject day1 "1.0.0"
  :description "Clojure solution to Day 1 puzzle from adventofcode.com"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/test.check "0.9.0"]]
  :main ^:skip-aot day1.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
