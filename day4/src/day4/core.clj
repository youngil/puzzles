(ns day4.core
  (:require [clojure.string :as str]))

(defn check-sum-from-name
  "Return the check sum of the encrypted string `name`.
  The checksum is the five most common letters in the encrypted name,
  in order, with ties broken by alphabetization.

  The sort order of the letters is determined first by frequency (in
  reverse order) then by alphatetical on letters.  This way we can
  just pick the first five letters in the list of common letters to
  get the right sequence for the checksum.
  "
  [name]
  (let [fm (-> name
               (str/replace "-" "")
               frequencies)]
    (->> fm
         (into (sorted-map-by #(compare [(fm %2) %1] [(fm %1) %2])))
         keys
         (take 5)
         (apply str))))

(defn parse-room-data
  "Parse the input `line` and return a vector of room data: sector-id,
  check-sum from input, computed check sum, and name from the input
  line."
  [line]
  (let [[_ name sid check-sum] (re-find #"(.*)-(\d{3})\[(\w{5})\]" line)]
    [(Integer/parseInt sid) check-sum (check-sum-from-name name) name]))

(defn real-rooms
  "Return rooms with valid check sum parsed from `lines`."
  [lines]
  (->> lines
       (map parse-room-data)
       (filter (fn [[_ cs1 cs2 _]] (= cs1 cs2)))))

(defn shift-char
  "Shift lower case letter `c` forward by `shift-n` steps. Return space
  for dash so that names with dashes will be decrypted properly."
  [shift-n c]
  (cond
    (= c \-) \space                              ; special case
    (= c \space) \-                              ; to enable inverse
    :else (let [ref (int \a)]
            (char (+ ref (mod (+ shift-n (- (int c) ref)) 26))))))
  
(defn decrypt-name
  "Shift all the characters in the `room-name` by `shift-n`"
  [shift-n room-name]
  (->> room-name
       seq
       (map (partial shift-char shift-n))
       (apply str)))

(defn find-room
  "Return the sector id of the room from `rooms` whose decrypted name is `search-name`."
  [search-name rooms]
  (->> rooms
       (map (fn [[sid c1 c2 name]] [sid c1 c2 (decrypt-name sid name)]))
       (filter (fn [[_ _ _ name]] (= search-name name)))
       ffirst))

(defn -main
  "Read from `input-file` and compute the sum of sector ids of real
  rooms, and find the sector id of the room whose decrpted name is
  'northpole object storage'."
  [input-file]
  (let [rooms (-> input-file
                  slurp 
                  str/split-lines            
                  real-rooms)]
    (println "Day 4 Part 1: Sum of sector ids for real rooms is"
             (reduce + 0 (map first rooms)))
    (let [search "northpole object storage"]
      (printf "Day 4 Part 2: Sector id of room named '%s' is %d\n"
              search 
              (find-room search rooms)))))

