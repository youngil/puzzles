# Day 4

The program first reads the inputs and returns a sequence of real room
names by computing the checksum.

For part 1, the sum of the sector ids can easily be computed from the
list of rooms.

For part 2, we need to implement a shift cypher that applies to
lower-case letters.  The cypher code also translates "-" to space and
vice versa, to make later processing and testing easier since shifting
is reversible.

# To Run from Command Line

lein run input

lein test
