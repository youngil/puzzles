(ns day4.core-test
  (:require [clojure.test :refer :all]
            [day4.core :refer :all]
            [clojure.string :as str]
            [clojure.test.check :as tc]
            [clojure.test.check.clojure-test :refer (defspec)]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))

;;; test.check
(def sector-id (gen/choose 100 999))
(def char-lower (gen/fmap char (gen/choose 97 122)))
(def char-seq (gen/vector char-lower 5 10))
(def word (gen/let [a (gen/vector char-lower 5 10)] (str/join a)))
(def check-sum (gen/let [a (gen/vector char-lower 5)] (str/join a)))
(def base-name (gen/let [a (gen/vector word 4 6)] (str/join "-" a)))

(defspec parse-room-data-test 5
  (prop/for-all [base base-name
                 cs1 check-sum
                 sid1 sector-id]
                (let [in (str base "-" sid1 "[" cs1 "]")
                      [sid2 cs2 computed-cs name] (parse-room-data in)]
                  (and (integer? sid2)
                       (= cs1 cs2)
                       (= sid1 sid2)
                       (= base name)))))

(defspec shift-char-test 10
  (prop/for-all [c1 char-lower
                 n1 gen/pos-int]
                (let [c2 (shift-char n1 c1)
                      c3 (shift-char (- n1) c2)]
                  (and (char? c3)
                       (= c1 c3)))))

(defspec check-sum-from-name-test 5
  (prop/for-all [base base-name]
                (let [cs (check-sum-from-name base)]
                  (and (pos? (count cs))
                       (every? (set (seq base)) cs)))))

(defspec decrypt-name-test 10
  (prop/for-all [base base-name
                 shift-n gen/nat]
                (let [name1 (decrypt-name shift-n base)
                      name2 (decrypt-name (- shift-n) name1)]
                  (and (= base name2)))))

;;; clojure.test
(deftest check-sum-from-name-test
  (testing "room check sum"
    (is (= "abcwx" (check-sum-from-name "bbbbcccwxyzaaaaa")))))

(deftest parse-room-data-test
  (testing "sector test"
    (is (= [123 "kotgr" "abcwx" "aaaaaa-bbbb-ccc-w-x-y-z"]
           (parse-room-data "aaaaaa-bbbb-ccc-w-x-y-z-123[kotgr]")))))
