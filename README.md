# Overview

This repository contains Clojure solutions to three of the programming
puzzles from adventofcode.com.  They are in directories day1, day4,
and day10.

Following some feedback, the programs have been extensively rewritten
to be more idiomatic Clojure.

To run, cd to each directory and run leiningen:

> lein run input

to run the solution (input is the name of the input file), or

> lein test

to run the test.check and clojure.test tests.

The README.md in each directory provides comments on the approach.

# Testing

In order to be able to test the actual values before and after the
function calls, we implement generators for the different components
of the input.  Then in the properties the generated components are
used to create the test inputs, allowing us to compare the before and
after values.


  
